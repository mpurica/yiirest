<?php
 
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);
 
return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'modules' => [
        'v1' => [
            'basePath' => '@app/modules/v1',
            'class' => 'api\modules\v1\Module'   // here is our v1 modules
        ]
    ],
    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                                [
                    'class' => 'yii\rest\UrlRule',
                    //'controller' => ['v1/country', 'v1/city'], 
                    'controller' => 'v1/country', 
                   'extraPatterns' => [
                        'GET {id}/cities' => 'cities',
                        //'GET {id}/cities/{codigo}' => 'search',
                        ], 
                   'tokens' => [
                        '{id}'     =>  '<id:\\w+>',
                       // '{pais}'   =>  '<pais:\\w+>',
                        //'{codigo}' =>  '<codigo:\\d+>',
                         ],
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    //'controller' => ['v1/country', 'v1/city'], 
                    'controller' => 'v1/city', 
                   /* 'extraPatterns' => [
                        'GET cities' => 'Cities',
                        ],  */
                    'tokens' => [
                        '{id}' => '<id:\\w+>'
                    ],
                    //'except' => ['index'],

                ],
            ],
        ]
    ],
    'params' => $params,
];
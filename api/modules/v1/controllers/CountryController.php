<?php
 
namespace api\modules\v1\controllers;
 
use yii\rest\ActiveController;

use api\modules\v1\models\City;
 
/**
 * Country Controller API
 *
 * @author Budi Irawan <deerawan@gmail.com>
 */
class CountryController extends ActiveController
{
    public $modelClass = 'api\modules\v1\models\Country';
        public $serializer = [
        'class' => 'yii\rest\Serializer',
        'collectionEnvelope' => 'items',
    ];
    /*public function actionCities()
	{
    return City::findAll();
	}*/
	public function extraFields()
    {
    return ['cities'];
    }
    public function actionSearch($id){
        echo json_encode(array("codigo_pais" => $id, "dos" => "dos", "tres" => "tres"));
    }
      public function actionCities($id){
        $cities = City::find()->where(['country_code' => $id])->asArray()->all();
       // $cities = Country->getCitiesByCountry($id)
        echo json_encode($cities);

    }

}

/*
GET /countries: list all countries
HEAD /countries: show the overview information of country listing
POST /countries: create a new country
GET /countries/AU: return the details of the country AU
HEAD /countries/AU: show the overview information of country AU
PATCH /countries/AU: update the country AU
PUT /countries/AU: update the country AU
DELETE /countries/AU: delete the country AU
OPTIONS /countries: show the supported verbs regarding endpoint /countries
OPTIONS /countries/AU: show the supported verbs regarding endpoint /countries/AU.
*/